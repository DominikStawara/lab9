package com.company;


/*wypisanie hello world*/
/*
class Main{
    public static void main(String[] args){
        System.out.println("Hello world");
    }
}
*/

public class Main{
    public Main(){
        System.out.println("Działa kostruktor klasy Main");
    }

    private Long getFactorial(final long digit){
        if (digit < 0) return Long.valueOf(0);
        else{
            long wynik = 1;
            for (long i = digit; i > 0; i--) {
                wynik *= i;
            }
            return wynik;
        }
    }

    public static void main(String[] args){
        long digit = 4;
        Main main = new Main();
        long factorial = main.getFactorial(digit);
        System.out.printf("Silnia(%d) = %d \n", digit, factorial);
   }
}